# GitLab C2 Agent

This project is linked to the Tech Note "[GitLab as a C2](https://gitlab.com/gitlab-com/gl-security/threatmanagement/redteam/redteam-public/red-team-tech-notes/-/tree/master/GitLab-as-C2)"

It contains the code of the PoC agent, more explanations and descriptions are contained within the Tech Note.

## Installation

The project has not external golang dependencies so you can:

- clone the repo or just download the golang code
- change the related constant variables at the top of the code to adapt to your environement (apiURL, agentName, projectID)
- add a project token within the main function (line 195 `accessToken=`) or for testing pass one as a $GITLAB_TOKEN env variable
- compile or directly execute the agent: `go run c2-agent.go` (or `GITLAB_TOKEN="glpat-<xxxx>" go run c2-agent.go`)

The agent will then create an issue titled from its name and you can interact with the agent within the comments of the issue.